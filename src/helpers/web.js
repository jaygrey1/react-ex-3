
const method = (request, sendMethod = 'get') => {
  return { ...request, method: sendMethod.toUpperCase() };
};

const authorized = request => {
  const backendToken = window.localStorage.getItem('backendToken');
  return { ...request, headers: { ...request.headers, 'x-access-token': backendToken } };
};

const body = (request, payload, type = 'json') => {
  switch (type) {
    case 'json': {
      return {
        ...request,
        headers: { ...request.headers, 'Content-Type': 'application/json' },
        body: JSON.stringify(payload),
      };
    }
    case 'form': {
      return {
        ...request,
        headers: { ...request.headers, 'Content-Type': 'application/x-www-form-urlencoded' },
        body: encodeURI(
          Object.entries(payload)
            .map(entry => entry.join('='))
            .join('&')
        ),
      };
    }
    default: {
      return request;
    }
  }
};

const receive = (request = {}, type = 'json') => {
  switch (type) {
    case 'json': {
      return { ...request, headers: { ...request.headers, accept: 'application/json' } };
    }
    default: {
      return request;
    }
  }
};

const send = async (request, server) => {
  const result = await window.fetch(server, request);

  if (!result.ok) {
    const errorMessage = (await result.json()).error;
    throw new Error(errorMessage);
  }

  return result.json();
};

export const postOrThrow = async (server, payload, type) =>
  send(method(body(receive(), payload, type), 'post'), server);

export const getOrThrow = async server => send(method(receive()), server);

export const postAuthorizedOrThrow = async (server, payload) =>
  send(method(authorized(body(receive(), payload)), 'post'), server);

export const putAuthorizedOrThrow = async (server, payload) =>
  send(method(authorized(body(receive(), payload)), 'put'), server);

export const getAuthorizedOrThrow = async server => send(method(authorized(receive())), server);

export const deleteAuthorizedOrThrow = async server =>
  send(method(authorized(receive()), 'delete'), server);
