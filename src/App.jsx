import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import HeaderContainer from './containers/HeaderContainer';
import HomePage from './pages/HomePage';
import ViewArticlePage from './pages/ViewArticlePage';
import UpdateArticlePage from './pages/UpdateArticlePage';
import AddArticlePage from './pages/AddArticlePage';
import DeleteArticlePage from './pages/DeleteArticlePage';
import PageNotFound from './pages/PageNotFound';
import LoginPage from './pages/LoginPage';
import Footer from './components/Footer';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

class App extends React.Component {
  componentDidMount() {
    if (window.gapi) {
      window.gapi.load('auth2', async () => {
        await window.gapi.auth2.init({
          client_id: process.env.REACT_APP_GOOGLE_CLIENT_ID,
        });
      });
    }
  }

  render() {
    return (
      <Router>
        <div className="App">
          <HeaderContainer />
          <Switch>
            <Route path={['/', '/news']} exact component={HomePage} />
            <Route path={['/signin', '/signup']} exact component={LoginPage} />
            <Route path="/news/add/" exact component={AddArticlePage} />
            <Route path="/news/:articleId([0-9a-z]+)/" exact component={ViewArticlePage} />
            <Route path="/news/:articleId([0-9a-z]+)/edit/" exact component={UpdateArticlePage} />
            <Route path="/news/:articleId([0-9a-z]+)/delete/" exact component={DeleteArticlePage} />
            <Route component={PageNotFound} />
          </Switch>
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
