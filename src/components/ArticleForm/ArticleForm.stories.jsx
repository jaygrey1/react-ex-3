import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions';
import { ArticleForm } from './ArticleForm';

const propsWithCustomText = {
  title: 'Article title',
  text: 'Article text',
  submitButtonText: 'Обновить',
  cancelButtonText: 'Отменить',
};

const actions = {
  submitAction: action('submitAction'),
};

storiesOf('ArticleForm', module)
  .addDecorator(story => (
    <div className="container">
      <div className="row justify-content-center mt-4">
        <div className="col-6">{story()}</div>
      </div>
    </div>
  ))
  .add('Default', () => <ArticleForm />)
  .add('Customized', () => <ArticleForm {...propsWithCustomText} {...actions} />);
