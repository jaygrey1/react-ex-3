import React from 'react';
import { shallow } from 'enzyme';
import { ArticleForm } from './ArticleForm';

describe('ArticleForm component', () => {
  it('renders correctly', () => {
    const component = shallow(<ArticleForm />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly when customized', () => {
    const props = {
      title: 'Article title',
      text: 'Article text',
      submitButtonText: 'Обновить',
      cancelButtonText: 'Отменить',
    };

    const component = shallow(<ArticleForm {...props} />);
    expect(component).toMatchSnapshot();
  });
});
