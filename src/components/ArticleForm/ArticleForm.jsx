import React, { useState } from 'react';
import PropTypes from 'prop-types';

const isTitleValid = title => title && title.length > 0;
const isTextValid = text => text && text.length > 0;
const isFormValid = (title, text) => isTitleValid(title) && isTextValid(text);

export const ArticleForm = props => {
  const [title, setTitle] = useState(props.title);
  const [text, setText] = useState(props.text);
  const { submitButtonText, cancelButtonText } = props;

  return (
    <div className="border rounded border-secondary mt-2 p-4">
      <form className="justify-content-start">
        <div className="form-group">
          <label htmlFor="ArticleForm_title">Title</label>
          <input
            type="text"
            id="ArticleForm_title"
            value={title}
            className="form-control w-75"
            onChange={e => {
              setTitle(e.currentTarget.value);
              if (isTitleValid(e.currentTarget.value)) {
                e.currentTarget.classList.remove('is-invalid');
              } else {
                e.currentTarget.classList.add('is-invalid');
              }
            }}
          />
        </div>
        <div className="form-group mt-4">
          <label htmlFor="ArticleForm_text">Text</label>
          <textarea
            id="ArticleForm_text"
            value={text}
            className="form-control mb-4"
            rows="10"
            onChange={e => {
              setText(e.currentTarget.value);
              if (isTextValid(e.currentTarget.value)) {
                e.currentTarget.classList.remove('is-invalid');
              } else {
                e.currentTarget.classList.add('is-invalid');
              }
            }}
          />
        </div>
        <div className="form-group">
          <button
            type="button"
            className="btn btn-primary btn-sm font-weight-bold"
            disabled={!isFormValid(title, text)}
            onClick={() => props.submitAction(title, text)}
          >
            {submitButtonText}
          </button>

          <button
            type="button"
            className="btn btn-primary btn-sm font-weight-bold ml-4"
            onClick={props.cancelAction}
          >
            {cancelButtonText}
          </button>
        </div>
      </form>
    </div>
  );
};

ArticleForm.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  submitButtonText: PropTypes.string,
  cancelButtonText: PropTypes.string,
  submitAction: PropTypes.func,
  cancelAction: PropTypes.func,
};

ArticleForm.defaultProps = {
  title: '',
  text: '',
  submitButtonText: 'Submit',
  cancelButtonText: 'Cancel',
  submitAction: () => {},
  cancelAction: () => {},
};
