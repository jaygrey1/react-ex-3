import React from 'react';

import { shallow } from 'enzyme';
import { Header } from './Header';

describe('Header component', () => {
  const defaultProps = {
    isAuthorized: false,
    isLoading: false,
    name: '',
    signIn: () => {},
    signOut: () => {},
  };

  it('renders properly', () => {
    const props = {
      ...defaultProps,
    };
    const component = shallow(<Header {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('calls signIn function when LogIn link clicked', () => {
    const mockSignIn = jest.fn();

    const props = {
      ...defaultProps,
      signIn: mockSignIn,
    };

    const component = shallow(<Header {...props} />);
    component
      .find('button')
      .filterWhere(node => node.text() === 'Войти')
      .first()
      .simulate('click');
    expect(mockSignIn).toHaveBeenCalledTimes(1);
  });

  it('displays user name and logout link when user is authorized', () => {
    const props = {
      ...defaultProps,
      isAuthorized: true,
      name: 'user',
    };

    const component = shallow(<Header {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('calls signOut function when when LogOut link clicked', () => {
    const mockSignOut = jest.fn();
    const props = {
      ...defaultProps,
      isAuthorized: true,
      name: 'user',
      signOut: mockSignOut,
    };
    const component = shallow(<Header {...props} />);
    component
      .find('button')
      .first()
      .simulate('click');
    expect(mockSignOut).toHaveBeenCalledTimes(1);
  });

  it('blocks LogIn link in the middle of request', () => {
    const mockSignIn = jest.fn();

    const props = {
      ...defaultProps,
      signIn: mockSignIn,
      isLoading: true,
    };

    const component = shallow(<Header {...props} />);
    expect(
      component
        .find('button')
        .first()
        .text()
    ).toBe('Войти');

    component
      .find('button')
      .first()
      .simulate('click');

    expect(mockSignIn).toHaveBeenCalledTimes(0);
  });
});
