import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export const Header = ({ isAuthorized, name, signOut }) => {
  const buttons = isAuthorized ? (
    <React.Fragment>
      <span className="navbar-text">{name}</span>
      <span className="navbar-text ml-3 mr-3">|</span>
      <button type="button" className="btn btn-link pl-0" onClick={signOut}>
        Выйти
      </button>
    </React.Fragment>
  ) : (
    <Link to="/signin">Войти</Link>
  );

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <ul className="nav mr-auto">
        <li className="nav-item">
          <Link to="/news" className="nav-link">
            Главная
          </Link>
        </li>
        {isAuthorized && (
          <li className="nav-item">
            <Link to="/news/add" className="nav-link">
              Добавить
            </Link>
          </li>
        )}
      </ul>
      {buttons}
    </nav>
  );
};

Header.propTypes = {
  isAuthorized: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
  signOut: PropTypes.func.isRequired,
};
