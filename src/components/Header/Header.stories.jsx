import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions';
import { Header } from './Header';

const defaultProps = {
  isAuthorized: false,
  isLoading: false,
  name: '',
  signIn: () => {},
  signOut: () => {},
};

const authorizedProps = {
  ...defaultProps,
  isAuthorized: true,
  name: 'some user',
};

const loadingProps = {
  ...defaultProps,
  isLoading: true,
};

const actions = {
  signIn: action('signIn'),
  signOut: action('signOut'),
};

storiesOf('Header', module)
  .addDecorator(story => <Router>{story()}</Router>)
  .add('Unauthorized', () => <Header {...defaultProps} {...actions} />)
  .add('Loading', () => <Header {...loadingProps} {...actions} />)
  .add('Authorized', () => <Header {...authorizedProps} {...actions} />);
