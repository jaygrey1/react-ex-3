import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { SignUpForm } from './SignUpForm';

const defaultProps = {};

const isLoadingProps = {
  isLoading: true,
};

const errorMessageProps = {
  errorMessage: 'some error occured some error occured some error occured some error occured',
};

storiesOf('SignUpForm', module)
  .addDecorator(story => (
    <Router>
      <div className="container">
        <div className="row justify-content-center mt-4">{story()}</div>
      </div>
    </Router>
  ))
  .add('Default', () => <SignUpForm {...defaultProps} />)
  .add('Loading', () => <SignUpForm {...isLoadingProps} />)
  .add('Error message', () => <SignUpForm {...errorMessageProps} />);
