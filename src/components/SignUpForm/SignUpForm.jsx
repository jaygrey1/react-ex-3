import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

import PermIdentity from '@material-ui/icons/PermIdentity';
import Lock from '@material-ui/icons/Lock';

import styles from './SignUpForm.module.css';

const isUsernameValid = username => username && username.length > 0;

const isPasswordValid = password => password && password.length > 5;

const isFormValide = (username, password1, password2, captcha) =>
  isUsernameValid(username) &&
  isPasswordValid(password1) &&
  password1 === password2 &&
  captcha.length > 0;

export const SignUpForm = props => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  const [captcha, setCaptcha] = useState('');

  const { isLoading, errorMessage, signUp } = props;

  useEffect(() => {
    if (window.grecaptcha && document.getElementById('gcaptcha').children.length === 0) {
      window.grecaptcha.render('gcaptcha', {
        sitekey: '6Lc-8K4UAAAAAODaQxRX06JutEFKdbM6U-yRqufU',
        theme: 'light',
        size: 'normal',
        callback: response => {
          setCaptcha(response);
        },
        'expired-callback': () => {
          setCaptcha('');
        },
        'error-callback': () => {
          setCaptcha('');
        },
      });
    }
  });

  return (
    <form className={[styles.SignUpForm, 'p-4'].join(' ')} noValidate>
      <h3 className={[styles.Header, 'text-center'].join(' ')}>Sign Up</h3>

      <div className="input-group mt-4">
        <div className="input-group-prepend">
          <span className="input-group-text">
            <PermIdentity />
          </span>
        </div>
        <input
          type="text"
          className="form-control"
          value={username}
          disabled={isLoading}
          placeholder="Username"
          required
          onChange={e => {
            setUsername(e.currentTarget.value);
            if (isUsernameValid(e.currentTarget.value)) {
              e.currentTarget.classList.remove('is-invalid');
            } else {
              e.currentTarget.classList.add('is-invalid');
            }
          }}
        />
      </div>

      <div className="input-group mt-4">
        <div className="input-group-prepend">
          <span className="input-group-text">
            <Lock />
          </span>
        </div>
        <input
          type="password"
          className="form-control"
          value={password}
          disabled={isLoading}
          placeholder="Password"
          required
          onChange={e => {
            setPassword(e.currentTarget.value);
            if (isPasswordValid(e.currentTarget.value)) {
              e.currentTarget.classList.remove('is-invalid');
            } else {
              e.currentTarget.classList.add('is-invalid');
            }
          }}
        />
      </div>

      <div className="input-group mt-4">
        <div className="input-group-prepend">
          <span className="input-group-text">
            <Lock />
          </span>
        </div>
        <input
          type="password"
          className="form-control"
          value={password2}
          disabled={isLoading}
          placeholder="Retype password"
          required
          onChange={e => {
            setPassword2(e.currentTarget.value);
            if (password === e.currentTarget.value) {
              e.currentTarget.classList.remove('is-invalid');
            } else {
              e.currentTarget.classList.add('is-invalid');
            }
          }}
        />
      </div>

      <div id="gcaptcha" className="mt-4 " />

      <p className={['text-justify', styles.Error].join(' ')}>{errorMessage}</p>

      <button
        type="button"
        disabled={isLoading || !isFormValide(username, password, password2, captcha)}
        className="btn btn-success btn-sm btn-block font-weight-bold mt-4"
        onClick={() => signUp(username, password, captcha)}
      >
        Sign Up
      </button>

      <p className="text-center mt-2">
        Already have an account? {isLoading ? 'Sign In' : <Link to="/signin">Sign In</Link>}
      </p>
    </form>
  );
};

SignUpForm.propTypes = {
  isLoading: PropTypes.bool,
  errorMessage: PropTypes.string,
  signUp: PropTypes.func,
};

SignUpForm.defaultProps = {
  isLoading: false,
  errorMessage: '',
  signUp: () => {},
};
