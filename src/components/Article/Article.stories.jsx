import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions';
import { Article } from './Article';

const longArticleText = `Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis architecto voluptates vel natus, cupiditate cum sunt ullam iste ipsum a labore ab porro molestiae, cumque deserunt, ea aspernatur placeat quia!
Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis architecto voluptates vel natus, cupiditate cum sunt ullam iste ipsum a labore ab porro molestiae, cumque deserunt, ea aspernatur placeat quia!
`;
const defaultProps = {
  article: {
    id: 1,
    author: 'some author',
    createdAt: '16.07.2019, 09:00:00',
    title: 'Article title',
    text: longArticleText,
    isOwner: false,
  },
};

const authorProps = {
  article: {
    ...defaultProps.article,
    isOwner: true,
  },
};

const defaultFullViewProps = {
  ...defaultProps,
  isPreview: false,
};

const ownerFullViewProps = {
  article: { ...defaultProps.article, isOwner: true },
  isPreview: false,
};

const actions = {
  onDeleteAction: action('onDeleteAction'),
  onCancelAction: action('onCancelAction'),
  onViewArticleAction: action('onViewArticleAction'),
};

storiesOf('Article', module)
  .addDecorator(story => (
    <Router>
      <div className="container">
        <div className="row justify-content-center mt-4">
          <div className="col-8">{story()}</div>
        </div>
      </div>
    </Router>
  ))
  .add('Default preview', () => <Article {...defaultProps} {...actions} />)
  .add('Author preview', () => <Article {...authorProps} {...actions} />)
  .add('Default full view', () => <Article {...defaultFullViewProps} {...actions} />)
  .add('Author full view', () => <Article {...ownerFullViewProps} {...actions} />);
