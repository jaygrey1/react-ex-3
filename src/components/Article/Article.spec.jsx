import React from 'react';
import { shallow } from 'enzyme';
import { Article } from './Article';

describe('Article component', () => {
  it('renders correctly in preview mode', () => {
    const props = {
      article: {
        id: '123',
        author: 'article author',
        createdAt: '14.07.2019, 15:26:06',
        title: 'article title',
        text: 'article text',
        isOwner: false,
      },
    };

    const component = shallow(<Article {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly in full view', () => {
    const props = {
      article: {
        id: '123',
        author: 'article author',
        createdAt: '14.07.2019, 15:26:06',
        title: 'article title',
        text: 'article text',
        isOwner: false,
      },
      isPreview: false,
    };

    const component = shallow(<Article {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly in author mode', () => {
    const props = {
      article: {
        id: '123',
        author: 'article author',
        createdAt: '14.07.2019, 15:26:06',
        title: 'article title',
        text: 'article text',
        isOwner: true,
      },
    };

    const component = shallow(<Article {...props} />);
    expect(component).toMatchSnapshot();
  });

  it.todo('has an author and created/modified date and time bar below the header');

  it.todo('update button follows to updated /news/:id in edit mode');
  it.todo('cancel button follows back to /news/:id in full view mode');

  it.todo('shows no more than 200 chars of of article text in preview mode');
  it.todo('shows edit and delete buttons in full view mode');
  it.todo('edit button follows to /news/:id/edit in full view mode');
  it.todo('delete button follows to /news page');
});
