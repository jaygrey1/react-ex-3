import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';

export const Article = props => {
  const {
    article: { id, title, author, text, isOwner, createdAt },
    isPreview,
  } = props;

  const titleButtons = isOwner && (
    <span>
      <Link to={`/news/${id}/edit/`}>
        <Edit />
      </Link>

      <Link to={`/news/${id}/delete`} className="ml-2">
        <Clear />
      </Link>
    </span>
  );

  const editButtons = !isPreview && isOwner && (
    <div>
      <Link to={`/news/${id}/edit`} className="btn btn-primary btn-sm font-weight-bold">
        Редактировать
      </Link>
      <Link to={`/news/${id}/delete`} className="btn btn-primary btn-sm font-weight-bold ml-4">
        Удалить
      </Link>
    </div>
  );

  return (
    <div className="card border-secondary mt-2">
      <div className="card-body">
        <div className="card-title d-flex mb-0">
          <Link to={`/news/${id}/`} className="text-decoration-none h4 mr-auto">
            {title}
          </Link>
          {titleButtons}
        </div>

        <small className="sub-title mb-4 text-muted">{`${author} | ${createdAt}`}</small>

        <p className="card-text text-justify">
          {isPreview ? text.substring(0, 200).concat(' ...') : text}
        </p>

        {editButtons}
      </div>
    </div>
  );
};

Article.propTypes = {
  article: PropTypes.shape({
    id: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    isOwner: PropTypes.bool.isRequired,
  }).isRequired,
  isPreview: PropTypes.bool,
};

Article.defaultProps = {
  isPreview: true,
};
