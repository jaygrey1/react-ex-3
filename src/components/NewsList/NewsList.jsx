import React from 'react';
import PropTypes from 'prop-types';

import Article from '../Article';
import Spinner from '../Spinner';

export const NewsList = ({ news, isLoading }) => {
  const articles = news.map(article => (
    <Article key={article.id} article={article} isOwner={article.isOwner} />
  ));

  return isLoading ? <Spinner /> : articles;
};

NewsList.propTypes = {
  news: PropTypes.arrayOf(PropTypes.any).isRequired,
  isLoading: PropTypes.bool,
};

NewsList.defaultProps = {
  isLoading: false,
};
