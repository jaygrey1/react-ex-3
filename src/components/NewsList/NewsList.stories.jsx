import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { NewsList } from './NewsList';

const defaultProps = {
  news: [
    {
      id: 1,
      author: 'author 1',
      title: 'title 1',
      text: 'text 1',
      createdAt: '16.07.2019, 09:00:00',
      isOwner: false,
    },
    {
      id: 2,
      author: 'author 2',
      title: 'title 2',
      text: 'text 2',
      createdAt: '16.07.2019, 09:00:00',
      isOwner: true,
    },
    {
      id: 3,
      author: 'author 3',
      title: 'title 3',
      text: 'text 3',
      createdAt: '16.07.2019, 09:00:00',
      isOwner: false,
    },
    {
      id: 4,
      author: 'author 4',
      title: 'title 4',
      text: 'text 4',
      createdAt: '16.07.2019, 09:00:00',
      isOwner: true,
    },
    {
      id: 5,
      author: 'author 5',
      title: 'title 5',
      text: 'text 5',
      createdAt: '16.07.2019, 09:00:00',
      isOwner: false,
    },
  ],
};

storiesOf('NewsList', module)
  .addDecorator(story => (
    <Router>
      <div className="container">
        <div className="row justify-content-center mt-4">
          <div className="col-8">{story()}</div>
        </div>
      </div>
    </Router>
  ))
  .add('default', () => <NewsList {...defaultProps} />)
  .add('loading', () => <NewsList isLoading news={[]} />);
