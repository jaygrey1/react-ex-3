import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { SignInForm } from './SignInForm';

const defaultProps = {};

const isLoadingProps = {
  isLoading: true,
};

const errorMessageProps = {
  errorMessage: 'some error occured some error occured some error occured some error occured',
};

storiesOf('SignInForm', module)
  .addDecorator(story => (
    <Router>
      <div className="container">
        <div className="row justify-content-center mt-4">{story()}</div>
      </div>
    </Router>
  ))
  .add('Default', () => <SignInForm {...defaultProps} />)
  .add('Loading', () => <SignInForm {...isLoadingProps} />)
  .add('Error message', () => <SignInForm {...errorMessageProps} />);
