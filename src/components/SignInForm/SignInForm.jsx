import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

import PermIdentity from '@material-ui/icons/PermIdentity';
import Lock from '@material-ui/icons/Lock';

import styles from './SignInForm.module.css';

const isUsernameValid = username => username && username.length > 0;

const isPasswordValid = password => password && password.length > 0;

const isFormValid = (username, password) => isUsernameValid(username) && isPasswordValid(password);

export const SignInForm = props => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const { isLoading, errorMessage, signIn, signInWithGoogle } = props;
  return (
    <form className={[styles.SignInForm, 'p-4'].join(' ')}>
      <h3 className={[styles.Header, 'text-center'].join(' ')}>Sign In</h3>

      <div className="input-group mt-4">
        <div className="input-group-prepend">
          <span className="input-group-text">
            <PermIdentity />
          </span>
        </div>
        <input
          type="text"
          className="form-control"
          value={username}
          disabled={isLoading}
          placeholder="Username"
          onChange={e => {
            setUsername(e.currentTarget.value);
            if (isUsernameValid(e.currentTarget.value)) {
              e.currentTarget.classList.remove('is-invalid');
            } else {
              e.currentTarget.classList.add('is-invalid');
            }
          }}
        />
      </div>

      <div className="input-group mt-4">
        <div className="input-group-prepend">
          <span className="input-group-text">
            <Lock />
          </span>
        </div>
        <input
          type="password"
          className="form-control"
          value={password}
          disabled={isLoading}
          placeholder="Password"
          onChange={e => {
            setPassword(e.currentTarget.value);
            if (isPasswordValid(e.currentTarget.value)) {
              e.currentTarget.classList.remove('is-invalid');
            } else {
              e.currentTarget.classList.add('is-invalid');
            }
          }}
        />
      </div>

      <p className={['text-justify', styles.Error].join(' ')}>{errorMessage}</p>

      <button
        type="button"
        disabled={isLoading}
        className="btn btn-primary btn-sm btn-block font-weight-bold mt-2"
        onClick={signInWithGoogle}
      >
        Sign In with Google
      </button>

      <button
        type="button"
        disabled={isLoading || !isFormValid(username, password)}
        className="btn btn-success btn-sm btn-block font-weight-bold mt-4"
        onClick={() => signIn(username, password)}
      >
        Sign In
      </button>

      <p className="text-center mt-2">
        Don&apos;t have an account? {isLoading ? 'Sign Up' : <Link to="/signup">Sign Up</Link>}
      </p>
    </form>
  );
};

SignInForm.propTypes = {
  isLoading: PropTypes.bool,
  errorMessage: PropTypes.string,
  signIn: PropTypes.func,
  signInWithGoogle: PropTypes.func,
};

SignInForm.defaultProps = {
  isLoading: false,
  errorMessage: '',
  signIn: () => {},
  signInWithGoogle: () => {},
};
