import React from 'react';

export const Footer = () => {
  return (
    <footer className="navbar navbar-light bg-light small fixed-bottom">
      <span className="navbar-text">
        Без воды, тестовое задание # 3 | Автор:
        <a href="mailto:jaygrey@yandex.ru">JayGrey</a>
      </span>
    </footer>
  );
};
