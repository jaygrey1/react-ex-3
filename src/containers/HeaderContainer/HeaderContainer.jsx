import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Header from '../../components/Header';
import { signIn, signOut } from '../../store/actions/user';
import * as userService from '../../services/user';
import * as authService from '../../services/auth';
import * as userSelector from '../../store/selectors/user';

class HeaderContainer extends React.Component {
  componentDidMount() {
    this.props.restoreUser();
  }

  render() {
    return <Header {...this.props} />;
  }
}

HeaderContainer.propTypes = {
  isAuthorized: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
  signOut: PropTypes.func.isRequired,
  restoreUser: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isAuthorized: userSelector.isAuthorized(state),
  isLoading: userSelector.isLoading(state),
  name: userSelector.getUserName(state),
});

const mapDispatchToProps = {
  signOut: () => signOut(authService.signOut),
  restoreUser: () => signIn(userService.restoreUser),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderContainer);
