import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import NewsList from '../../components/NewsList';
import Spinner from '../../components/Spinner';

import * as newsSelector from '../../store/selectors/news';
import { getNews as getNewsAction } from '../../store/actions/news';
import * as newsService from '../../services/news';

class HomePage extends React.Component {
  componentDidMount() {
    const { isFetched } = this.props;
    if (!isFetched) {
      this.props.getAllNews();
    }
  }

  render() {
    const { isLoading, news } = this.props;
    return (
      <div className="container">
        <div className="row justify-content-center mt-4">
          <div className="col-8">{isLoading ? <Spinner /> : <NewsList news={news} />}</div>
        </div>
      </div>
    );
  }
}

HomePage.propTypes = {
  news: PropTypes.arrayOf(PropTypes.any).isRequired,
  isLoading: PropTypes.bool.isRequired,
  getAllNews: PropTypes.func.isRequired,
  isFetched: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  news: newsSelector.getAllNews(state),
  isLoading: newsSelector.isNewsLoading(state),
  isFetched: newsSelector.isNewsFetched(state),
});

const mapDispatchToProps = {
  getAllNews: () => getNewsAction(newsService.getAllNews),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
