import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import ArticleForm from '../../components/ArticleForm';
import Spinner from '../../components/Spinner';
import {
  getArticle as getArticleAction,
  updateArticle as updateArticleAction,
} from '../../store/actions/news';
import * as newsSelector from '../../store/selectors/news';
import * as newsService from '../../services/news';

class UpdateArticlePage extends React.Component {
  componentDidMount() {
    const { article } = this.props;
    if (!article) {
      this.props.getArticleAction(this.props.match.params.articleId);
    }
  }

  onSubmitAction = (title, text) => {
    const { article, history, match } = this.props;
    this.props.updateArticleAction({ id: article.id, title, content: text }, () =>
      history.replace(`/news/${match.params.articleId}/`)
    );
  };

  render() {
    const { article, isLoading, history } = this.props;

    return (
      <div className="container">
        <div className="row justify-content-center mt-4">
          <div className="col-10">
            {isLoading || !article ? (
              <Spinner />
            ) : (
              <ArticleForm
                title={article.title}
                text={article.text}
                submitButtonText="Обновить"
                cancelButtonText="Отменить"
                submitAction={this.onSubmitAction}
                cancelAction={() => history.goBack()}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

UpdateArticlePage.propTypes = {
  article: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool.isRequired,
  getArticleAction: PropTypes.func.isRequired,
  updateArticleAction: PropTypes.func.isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
};

UpdateArticlePage.defaultProps = {
  article: undefined,
};

const mapStateToProps = (state, ownProps) => ({
  article: newsSelector.getArticle(ownProps.match.params.articleId, state),
  isLoading: newsSelector.isArticleLoading(state),
});

const mapDispatchToProps = {
  getArticleAction: articleId => getArticleAction(newsService.getArticle, articleId),
  updateArticleAction: (article, finalAction) =>
    updateArticleAction(newsService.updateArticle, article, finalAction),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateArticlePage);
