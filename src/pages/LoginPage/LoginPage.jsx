import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import SignInForm from '../../components/SignInForm';
import SignUpForm from '../../components/SignUpForm';

import { signIn } from '../../store/actions/user';
import * as authService from '../../services/auth';
import * as userSelector from '../../store/selectors/user';

const LoginPage = props => {
  if (props.isAuthorized) {
    props.history.replace('/news');
  }

  return (
    <div className="container">
      <div className="row justify-content-center mt-4">
        {props.match.url === '/signin' ? <SignInForm {...props} /> : <SignUpForm {...props} />}
      </div>
    </div>
  );
};

LoginPage.propTypes = {
  isAuthorized: PropTypes.bool.isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
  isAuthorized: userSelector.isAuthorized(state),
  errorMessage: userSelector.getErrorMessage(state),
});

const mapDispatchToProps = {
  signInWithGoogle: () => signIn(authService.signInWithGoogle),
  signIn: (username, password) => signIn(() => authService.signIn(username, password)),
  signUp: (username, password, captcha) =>
    signIn(() => authService.signUp(username, password, captcha)),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
