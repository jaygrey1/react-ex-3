import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { deleteArticle as deleteArticleAction } from '../../store/actions/news';
import { deleteArticle as deleteArticleService } from '../../services/news';

class DeleteArticlePage extends React.Component {
  componentDidMount() {
    const { match, history } = this.props;

    this.props.deleteArticle({ id: match.params.articleId }, () => history.replace('/news/'));
  }

  render() {
    return null;
  }
}

DeleteArticlePage.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  deleteArticle: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  deleteArticle: (article, finalAction) =>
    deleteArticleAction(deleteArticleService, article, finalAction),
};

export default connect(
  null,
  mapDispatchToProps
)(DeleteArticlePage);
