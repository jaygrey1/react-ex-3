import React from 'react';
import { Link } from 'react-router-dom';

export const PageNotFound = () => (
  <div>
    <h1 className="text-center">Page not found</h1>
    <p className="text-center">
      Back to
      <Link to="/news" className="pl-2">
        Home
      </Link>
    </p>
  </div>
);
