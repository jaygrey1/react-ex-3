import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Article from '../../components/Article';
import Spinner from '../../components/Spinner';
import * as newsSelector from '../../store/selectors/news';
import { getArticle as getArticleAction } from '../../store/actions/news';
import * as newsService from '../../services/news';

class ViewArticlePage extends React.Component {
  componentDidMount() {
    const { article } = this.props;
    if (!article) {
      this.props.getArticleAction(this.props.match.params.articleId);
    }
  }

  render() {
    const { article, isLoading } = this.props;

    return (
      <div className="container">
        <div className="row justify-content-center mt-4">
          <div className="col-10">
            {isLoading || !article ? <Spinner /> : <Article article={article} isPreview={false} />}
          </div>
        </div>
      </div>
    );
  }
}

ViewArticlePage.propTypes = {
  article: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool.isRequired,
  getArticleAction: PropTypes.func.isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired,
};

ViewArticlePage.defaultProps = {
  article: undefined,
};

const mapStateToProps = (state, ownProps) => ({
  article: newsSelector.getArticle(ownProps.match.params.articleId, state),
  isLoading: newsSelector.isArticleLoading(state),
});

const mapDispatchToProps = {
  getArticleAction: articleId => getArticleAction(newsService.getArticle, articleId),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewArticlePage);
