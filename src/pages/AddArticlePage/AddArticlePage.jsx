import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import ArticleForm from '../../components/ArticleForm';

import { addArticle as addArticleAction } from '../../store/actions/news';
import * as newsService from '../../services/news';

const AddArticlePage = props => {
  const { addArticle, history } = props;
  return (
    <div className="container ">
      <h2 className="text-center">Добавить статью</h2>

      <div className="row justify-content-center">
        <div className="col-6">
          <ArticleForm
            submitButtonText="Добавить"
            cancelButtonText="Отменить"
            submitAction={addArticle}
            cancelAction={() => history.replace('/news/')}
          />
        </div>
      </div>
    </div>
  );
};

AddArticlePage.propTypes = {
  addArticle: PropTypes.func.isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  addArticle: (title, content) =>
    addArticleAction(newsService.addArticle, { title, content }, () =>
      ownProps.history.replace('/news/')
    )(dispatch),
});

export default connect(
  undefined,
  mapDispatchToProps
)(AddArticlePage);
