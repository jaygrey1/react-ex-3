export const getUserId = state => state.user.id;

export const isAuthorized = state => state.user.isAuthorized;

export const isLoading = state => state.user.isLoading;

export const getUserName = state => state.user.name;

export const getErrorMessage = state => state.user.errorMessage;