import { getAllNews, isNewsFetched } from './news';

describe('news selector', () => {
  it('returns empty array if no news fetched from server', () => {
    const store = {
      news: {
        news: null,
      },
      user: {
        id: null,
      },
    };

    const actualValue = getAllNews(store);

    expect(actualValue).toEqual([]);
  });

  it('returns correct isFetched flag when feeds doesnt fetched from server', () => {
    const store = {
      news: {
        news: null,
      },
      user: {
        id: null,
      },
    };

    const actualValue = isNewsFetched(store);

    expect(actualValue).toBe(false);
  });

  it('returns correct isFetched flag when feeds fetched from server', () => {
    const store = {
      news: {
        news: [1, 2, 3],
      },
      user: {
        id: null,
      },
    };

    const actualValue = isNewsFetched(store);

    expect(actualValue).toBe(true);
  });
});
