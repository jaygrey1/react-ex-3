import { formatDate } from '../../helpers/utils';
import { getUserId } from './user';

const mapFeedToArticle = (currentUser, feed) => ({
  id: feed._id,
  title: feed.title,
  text: feed.content,
  author: feed.creator.displayName,
  createdAt: formatDate(feed.createDate),
  isOwner: currentUser === feed.creator._id,
});

export const getArticle = (articleId, state) => {
  const result = state.news.news ? state.news.news.filter(feed => feed._id === articleId) : null;
  if (result) {
    return mapFeedToArticle(getUserId(state), result[0]);
  }
  return null;
};

export const getAllNews = state => {
  const currentUserId = state.user.id;

  return state.news.news ? state.news.news.map(feed => mapFeedToArticle(currentUserId, feed)) : [];
};

export const isNewsLoading = state => state.news.isLoading;

export const isArticleLoading = state => state.news.isArticleLoading;

export const isNewsFetched = state => state.news.news !== null;
