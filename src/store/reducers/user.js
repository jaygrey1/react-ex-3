import * as t from '../actions/actionTypes';

const initialState = {
  id: '',
  name: '',
  isLoading: false,
  isAuthorized: false,
  errorMessage: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case t.SIGN_IN_REQUEST: {
      return { ...state, isLoading: true, errorMessage: '' };
    }

    case t.SIGN_IN_SUCCESS: {
      return {
        ...state,
        name: action.payload.name,
        id: action.payload.id,
        isLoading: false,
        isAuthorized: true,
        errorMessage: '',
      };
    }

    case t.SIGN_IN_FAILURE: {
      return { ...state, errorMessage: action.payload, isLoading: false };
    }

    case t.SIGN_OUT: {
      return { ...initialState };
    }

    default:
      return { ...state };
  }
};

export default reducer;
