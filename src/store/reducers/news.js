import * as t from '../actions/actionTypes';

const initialState = {
  news: null,
  isLoading: false,
  errorMessage: '',
  isArticleLoading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case t.GET_NEWS_REQUEST: {
      return { ...state, isLoading: true, errorMessage: '' };
    }

    case t.GET_NEWS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        news: action.payload,
        errorMessage: '',
      };
    }

    case t.GET_NEWS_FAILURE: {
      return { ...state, isLoading: false, errorMessage: action.payload };
    }

    case t.GET_ARTICLE_REQUEST:
    case t.ADD_ARTICLE_REQUEST:
    case t.UPDATE_ARTICLE_REQUEST: {
      return {
        ...state,
        isArticleLoading: true,
      };
    }

    case t.GET_ARTICLE_SUCCESS:
    case t.ADD_ARTICLE_SUCCESS: {
      return {
        ...state,
        news:
          state.news.filter(item => item._id === action.payload._id).length === 0
            ? [...state.news, action.payload]
            : state.news,
        isArticleLoading: false,
      };
    }

    case t.UPDATE_ARTICLE_SUCCESS: {
      return {
        ...state,
        news: state.news.map(item => (item._id === action.payload._id ? action.payload : item)),
        isArticleLoading: false,
      };
    }

    case t.DELETE_ARTICLE_SUCCESS: {
      return {
        ...state,
        news: state.news.filter(item => item._id !== action.payload._id),
      };
    }

    case t.GET_ARTICLE_FAILURE:
    case t.ADD_ARTICLE_FAILURE:
    case t.DELETE_ARTICLE_FAILURE:
    case t.UPDATE_ARTICLE_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload,
        isArticleLoading: false,
      };
    }

    default: {
      return { ...state };
    }
  }
};

export default reducer;
