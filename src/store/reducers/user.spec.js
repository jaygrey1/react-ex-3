import reducer from './user';
import * as t from '../actions/actionTypes';

const initialState = {
  id: '',
  name: '',
  isLoading: false,
  isAuthorized: false,
  errorMessage: '',
};

describe('user reducer', () => {
  it('returns initial state on unknown action', () => {
    expect(reducer(initialState, {})).toEqual(initialState);
  });

  it('sets isLoading on SIGN_IN_REQUEST action', () => {
    expect(reducer(initialState, { type: t.SIGN_IN_REQUEST })).toEqual({
      ...initialState,
      isLoading: true,
    });
  });

  it('sets isLoading on SIGN_IN_REQUEST action after failure', () => {
    const currentState = {
      ...initialState,
      errorMessage: 'some error',
    };
    expect(reducer(currentState, { type: t.SIGN_IN_REQUEST })).toEqual({
      ...currentState,
      isLoading: true,
      errorMessage: '',
    });
  });

  it('sets user name on SIGN_IN_SUCCESS action', () => {
    const payload = { name: 'username', id: 'someid' };
    const action = { type: t.SIGN_IN_SUCCESS, payload };

    const currentState = {
      ...initialState,
      isLoading: true,
    };

    const expectedState = {
      ...initialState,
      isLoading: false,
      name: action.payload.name,
      id: action.payload.id,
      isAuthorized: true
    };

    expect(reducer(currentState, action)).toEqual(expectedState);
  });

  it('sets errorMessage on SIGN_IN_FAILURE action', () => {
    const currentState = {
      ...initialState,
      isLoading: true,
    };

    const action = { type: t.SIGN_IN_FAILURE, payload: 'some_error' };
    expect(reducer(currentState, action)).toEqual({
      ...initialState,
      errorMessage: action.payload,
    });
  });
});
