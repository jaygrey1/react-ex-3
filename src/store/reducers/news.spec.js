import reducer from './news';
import * as t from '../actions/actionTypes';

describe('news reducer', () => {
  const initialState = {
    news: null,
    isLoading: false,
    errorMessage: '',
    isArticleLoading: false
  };

  it('returns initial state on unknown action', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('sets isLoading on GET_MESSAGE_REQUEST without previous error', () => {
    const action = {
      type: t.GET_NEWS_REQUEST,
    };
    const currentState = { ...initialState };
    const expectedState = {
      ...initialState,
      isLoading: true,
      errorMessage: '',
    };
    expect(reducer(currentState, action)).toEqual(expectedState);
  });

  it('sets isLoading on GET_MESSAGE_REQUEST with previous error', () => {
    const action = {
      type: t.GET_NEWS_REQUEST,
    };
    const currentState = { ...initialState, errorMessage: 'some error' };

    const expectedState = {
      ...initialState,
      isLoading: true,
      errorMessage: '',
    };
    expect(reducer(currentState, action)).toEqual(expectedState);
  });

  it('sets message on GET_MESSAGE_SUCCESS', () => {
    const mockedNews = [1, 2, 3];
    const currentState = {
      ...initialState,
      isLoading: true,
      news: [],
      errorMessage: '',
    };
    const expectedState = {
      ...currentState,
      isLoading: false,
      news: mockedNews,
      errorMessage: '',
    };

    const action = {
      type: t.GET_NEWS_SUCCESS,
      payload: mockedNews,
    };

    expect(reducer(currentState, action)).toEqual(expectedState);
  });

  it('sets errorMessage on GET_MESSAGE_FAILURE', () => {
    const currentState = {
      ...initialState,
      isLoading: true,
      news: [],
      errorMessage: '',
    };
    const expectedState = {
      ...currentState,
      isLoading: false,
      news: [],
      errorMessage: 'some error',
    };

    const action = {
      type: t.GET_NEWS_FAILURE,
      payload: 'some error',
    };

    expect(reducer(currentState, action)).toEqual(expectedState);
  });

  it('add new article on GET_ARTICLE_SUCCESS if its absent', () => {
    const stateBefore = {
      news: [{ _id: 1 }, { _id: 2 }, { _id: 3 }],
      isLoading: false,
      errorMessage: '',
      isArticleLoading: true,
    };

    const stateAfter = {
      news: [{ _id: 1 }, { _id: 2 }, { _id: 3 }, { _id: 4 }],
      isLoading: false,
      errorMessage: '',
      isArticleLoading: false
    };

    const action = { type: t.GET_ARTICLE_SUCCESS, payload: { _id: 4 } };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it('ignores new article on GET_ARTICLE_SUCCESS if it is present', () => {
    const stateBefore = {
      news: [{ _id: 1 }, { _id: 2 }, { _id: 3 }],
      isLoading: false,
      errorMessage: '',
      isArticleLoading: true,
    };

    const stateAfter = {
      news: [{ _id: 1 }, { _id: 2 }, { _id: 3 }],
      isLoading: false,
      errorMessage: '',
      isArticleLoading: false,
    };

    const action = { type: t.GET_ARTICLE_SUCCESS, payload: { _id: 2 } };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it('update article on UPDATE_ARTICLE_SUCCESS ', () => {
    const stateBefore = {
      news: [{ _id: 1 }, { _id: 2, title: 'title 2', content: 'content 2' }, { _id: 3 }],
      isLoading: false,
      errorMessage: '',
      isArticleLoading: true,
    };

    const stateAfter = {
      news: [
        { _id: 1 },
        { _id: 2, title: 'updated title 2', content: 'updated content 2' },
        { _id: 3 },
      ],
      isLoading: false,
      errorMessage: '',
      isArticleLoading: false,
    };

    const action = {
      type: t.UPDATE_ARTICLE_SUCCESS,
      payload: {
        _id: 2,
        title: 'updated title 2',
        content: 'updated content 2',
      },
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it('delete non selected article on DELETE_ARTICLE_SUCCESS', () => {
    const stateBefore = {
      news: [{ _id: 1 }, { _id: 2 }, { _id: 3 }],
      isLoading: false,
      errorMessage: '',
      selectedArticle: {
        isLoading: false,
        article: null,
      },
    };

    const stateAfter = {
      news: [{ _id: 1 }, { _id: 3 }],
      isLoading: false,
      errorMessage: '',
      selectedArticle: {
        isLoading: false,
        article: null,
      },
    };

    const action = {
      type: t.DELETE_ARTICLE_SUCCESS,
      payload: {
        _id: 2,
      },
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it('delete selected article on DELETE_ARTICLE_SUCCESS', () => {
    const stateBefore = {
      news: [{ _id: 1 }, { _id: 2 }, { _id: 3 }],
      isLoading: false,
      errorMessage: '',
      isArticleLoading: false,
    };

    const stateAfter = {
      news: [{ _id: 1 }, { _id: 3 }],
      isLoading: false,
      errorMessage: '',
      isArticleLoading: false,
    };

    const action = {
      type: t.DELETE_ARTICLE_SUCCESS,
      payload: {
        _id: 2,
      },
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });
});
