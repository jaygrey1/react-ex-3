import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { signIn, signOut } from './user';
import * as t from './actionTypes';

const middlewares = [thunk];
const store = configureStore(middlewares)({});

describe('Header container actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  it('fires SIGN_IN_REQUEST SIGN_IN_SUCCESS on success signIn action', async () => {
    const mockSignInService = jest.fn();
    const payload = { name: 'username', id: 'someid' };
    mockSignInService.mockResolvedValue(payload);

    await store.dispatch(signIn(mockSignInService));

    const expectedActions = [
      { type: t.SIGN_IN_REQUEST },
      { type: t.SIGN_IN_SUCCESS, payload },
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('fires SIGN_IN_REQUEST SIGN_IN_FAILURE on failure signIn action', async () => {
    const mockSignInService = jest.fn();
    const errorMessage = 'server error';
    mockSignInService.mockRejectedValue(new Error(errorMessage));

    await store.dispatch(signIn(mockSignInService));

    const expectedActions = [
      { type: t.SIGN_IN_REQUEST },
      { type: t.SIGN_IN_FAILURE, payload: errorMessage },
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });

  it('fires SIGN_OUT on signOut action', async () => {
    const mockSignInService = jest.fn();
    mockSignInService.mockResolvedValue({});
    
    await store.dispatch(signOut(mockSignInService));

    const actions = store.getActions();
    const expectedActions = [{ type: t.SIGN_OUT }];
    expect(actions).toEqual(expectedActions);
  });
});
