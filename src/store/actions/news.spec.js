import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { getNews } from './news';

import * as t from './actionTypes';

const middlewares = [thunk];
const store = configureStore(middlewares)({});

describe('News container actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  it('getNews() fires actions GET_NEWS_REQUEST then GET_NEWS_SUCCESS if request was successfull', async () => {
    const mockService = jest.fn();
    const mockedNews = [{ id: 1, title: 'title', text: 'text', isOwner: false }];
    mockService.mockResolvedValue(mockedNews);

    const expectedAction = [
      { type: t.GET_NEWS_REQUEST },
      { type: t.GET_NEWS_SUCCESS, payload: mockedNews },
    ];

    await store.dispatch(getNews(mockService));

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('getNews() fires actions GET_NEWS_REQUEST then GET_NEWS_FAILURE if request was failed', async () => {
    const mockService = jest.fn();
    const errorMessage = 'server error';
    mockService.mockRejectedValue(new Error(errorMessage));

    const expectedAction = [
      { type: t.GET_NEWS_REQUEST },
      { type: t.GET_NEWS_FAILURE, payload: errorMessage },
    ];

    await store.dispatch(getNews(mockService));

    expect(store.getActions()).toEqual(expectedAction);
  });
});
