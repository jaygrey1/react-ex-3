import * as t from './actionTypes';

const getNewsRequestAction = () => ({ type: t.GET_NEWS_REQUEST });
const getNewsSuccessAction = news => ({ type: t.GET_NEWS_SUCCESS, payload: news });
const getNewsFailureAction = message => ({ type: t.GET_NEWS_FAILURE, payload: message });

const addArticleRequestAction = () => ({ type: t.ADD_ARTICLE_REQUEST });
const addArticleSuccessAction = article => ({ type: t.ADD_ARTICLE_SUCCESS, payload: article });
const addArticleFailureAction = message => ({ type: t.ADD_ARTICLE_FAILURE, payload: message });

const getArticleRequestAction = () => ({ type: t.GET_ARTICLE_REQUEST });
const getArticleSuccessAction = article => ({ type: t.GET_ARTICLE_SUCCESS, payload: article });
const getArticleFailureAction = message => ({ type: t.GET_ARTICLE_FAILURE, payload: message });

const updateArticleRequestAction = () => ({ type: t.UPDATE_ARTICLE_REQUEST });
const updateArticleSuccessAction = article => ({
  type: t.UPDATE_ARTICLE_SUCCESS,
  payload: article,
});
const updateArticleFailureAction = message => ({
  type: t.UPDATE_ARTICLE_FAILURE,
  payload: message,
});

const deleteArticleSuccessAction = article => ({
  type: t.DELETE_ARTICLE_SUCCESS,
  payload: article,
});

const deleteArticleFailureAction = message => ({
  type: t.DELETE_ARTICLE_FAILURE,
  payload: message,
});

export const getNews = service => async dispatch => {
  dispatch(getNewsRequestAction());

  try {
    const result = await service();
    dispatch(getNewsSuccessAction(result));
  } catch (error) {
    dispatch(getNewsFailureAction(error.message));
  }
};

export const addArticle = (service, article, finalAction = () => {}) => async dispatch => {
  dispatch(addArticleRequestAction());
  try {
    const result = await service(article);
    dispatch(addArticleSuccessAction(result));
  } catch (error) {
    dispatch(addArticleFailureAction(error.message));
  } finally {
    finalAction();
  }
};

export const getArticle = (service, articleId) => async dispatch => {
  dispatch(getArticleRequestAction());
  try {
    const result = await service(articleId);
    dispatch(getArticleSuccessAction(result));
  } catch (error) {
    dispatch(getArticleFailureAction(error.message));
  }
};

export const updateArticle = (service, article, finalAction) => async dispatch => {
  dispatch(updateArticleRequestAction());
  try {
    const result = await service(article);
    dispatch(updateArticleSuccessAction(result));
  } catch (error) {
    dispatch(updateArticleFailureAction(error.message));
  } finally {
    finalAction();
  }
};

export const deleteArticle = (service, article, finalAction = () => {}) => async dispatch => {
  try {
    const result = await service(article);
    dispatch(deleteArticleSuccessAction(result));
  } catch (error) {
    dispatch(deleteArticleFailureAction(error.message));
  } finally {
    finalAction();
  }
};
