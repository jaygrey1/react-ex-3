import * as t from './actionTypes';

const signInRequestAction = () => ({
  type: t.SIGN_IN_REQUEST,
});

const signInSuccessAction = payload => ({
  type: t.SIGN_IN_SUCCESS,
  payload,
});

const signInFailureAction = message => ({
  type: t.SIGN_IN_FAILURE,
  payload: message,
});

const signOutAction = () => ({
  type: t.SIGN_OUT,
});

/**
 *
 * @param {function} service
 * @returns {object} object with name field
 */
export const signIn = service => async dispatch => {
  dispatch(signInRequestAction());

  try {
    const result = await service();
    dispatch(signInSuccessAction(result));
  } catch (error) {
    dispatch(signInFailureAction(error.message));
  }
};

/**
 * Sign out action
 * @param {function} service
 * @returns {object} object with name field
 */
export const signOut = service => async dispatch => {
  await service();
  dispatch(signOutAction());
};
