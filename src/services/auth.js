import { postOrThrow } from '../helpers/web';
import { getUserInfo } from './user';

export const signInWithGoogle = async () => {
  const googleAuth = window.gapi.auth2.getAuthInstance();

  // sign to google
  const googleUser = await googleAuth.signIn();
  const googleTokenId = googleUser.getAuthResponse().id_token;
  window.localStorage.setItem('googleToken', googleTokenId);

  // send google id_token to /auth/google
  const backendToken = (await postOrThrow(
    `${process.env.REACT_APP_BACKEND_SERVER}/auth/google`,
    { token: googleTokenId },
    'form'
  )).token;

  // save token to local storage
  window.localStorage.setItem('backendToken', backendToken);

  return getUserInfo();
};

export const signIn = async (username, password) => {
  const backendToken = (await postOrThrow(`${process.env.REACT_APP_BACKEND_SERVER}/auth`, {
    username,
    password,
  })).token;
  window.localStorage.setItem('backendToken', backendToken);
  return getUserInfo();
};

export const signOut = async () => {
  const googleAuth = window.gapi.auth2.getAuthInstance();
  await googleAuth.signOut();
  window.localStorage.clear();
};

export const signUp = async (username, password, captcha) => {
  const backendToken = (await postOrThrow(`${process.env.REACT_APP_BACKEND_SERVER}/users`, {
    username,
    password,
    'g-recaptcha-response': captcha
  })).token;
  window.localStorage.setItem('backendToken', backendToken);
  return getUserInfo();
};