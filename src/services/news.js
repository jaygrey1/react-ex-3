import * as webApi from '../helpers/web';

const server = process.env.REACT_APP_BACKEND_SERVER;

export const getAllNews = async () => {
  const result = await webApi.getOrThrow(`${server}/feeds`);

  return result.feeds;
};

export const addArticle = async article => {
  const result = await webApi.postAuthorizedOrThrow(`${server}/feeds/`, article);

  return result.feed;
};

export const getArticle = async articleId => {
  const result = await webApi.getOrThrow(`${server}/feeds/${articleId}`);

  return result.feed;
};

export const updateArticle = async article => {
  const result = await webApi.putAuthorizedOrThrow(`${server}/feeds/${article.id}`, {
    title: article.title,
    content: article.content,
  });
  return result.feed;
};

export const deleteArticle = async article => {
  return webApi.deleteAuthorizedOrThrow(`${server}/feeds/${article.id}`);
};
