import { decode } from 'jws';

import { getAuthorizedOrThrow, postOrThrow } from '../helpers/web';

const isExpired = timeInSec => timeInSec * 1000 <= new Date().getTime();

export const getUserInfo = async () => {
  // send to backend get: /users/{id} extracting id from backed access token earlier, to get user name
  const backendToken = window.localStorage.getItem('backendToken');
  const userId = decode(backendToken).payload.id;

  const result = await getAuthorizedOrThrow(
    `${process.env.REACT_APP_BACKEND_SERVER}/users/${userId}`
  );

  return { id: result.user._id, name: result.user.displayName };
};

export const restoreUser = async () => {
  const backendToken = window.localStorage.getItem('backendToken');

  if (!backendToken) {
    throw new Error();
  }

  if (!isExpired(decode(backendToken).payload.exp)) {
    const userId = decode(backendToken).payload.id;
    const authorizedUser = await getAuthorizedOrThrow(
      `${process.env.REACT_APP_BACKEND_SERVER}/users/${userId}`
    );
    return { id: authorizedUser.user._id, name: authorizedUser.user.displayName };
  }

  const googleToken = window.localStorage.getItem('googleToken');
  if (!googleToken) {
    throw new Error();
  }

  if (!isExpired(decode(googleToken).payload.exp)) {
    // post from /auth/google
    const newBackendToken = postOrThrow(
      `${process.env.REACT_APP_BACKEND_SERVER}/auth/google`,
      { token: googleToken },
      'form'
    );
    window.localStorage.setItem('backendToken', newBackendToken);

    // get from /users/{id}
    const userId = decode(newBackendToken).payload.id;
    return getAuthorizedOrThrow(`${process.env.REACT_APP_BACKEND_SERVER}/users/${userId}`);
  }

  throw new Error();
};