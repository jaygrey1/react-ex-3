import { configure } from '@storybook/react';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';


const req = require.context('../src/components', true, /\.stories\.jsx$/);

const loadStories = () => {
  req.keys().forEach(filename => req(filename));
};

configure(loadStories, module);
